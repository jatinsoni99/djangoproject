# Generated by Django 3.1.7 on 2021-04-16 10:15

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_auto_20210416_1519'),
    ]

    operations = [
        migrations.RenameField(
            model_name='product',
            old_name='selling_price',
            new_name='discounte_price',
        ),
    ]
