from django.urls import path
from app import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_view
from django.views.generic.base import TemplateView
from .forms import LoginForm,RegistrationForm,ChangePassword,ResetForm,SetPassword
urlpatterns = [
    path('', views.Home.as_view()),
    path('product-detail/<int:pk>', views.ProductDetail.as_view(), name='product-detail'),
    path('add-to-cart/', views.add_to_cart, name='add-to-cart'),
    path('cart/', views.showcart, name='showcart'),
    path('pluscart/', views.pluscart, name='pluscart'),  
    path('minuscart/', views.minuscart, name='minuscart'),    
    path('removecart/', views.removecart, name='removecart'),    


    path('buy/', views.buy_now, name='buy-now'),
    path('profile/', views.Profile.as_view(), name='profile'),
    path('address/', views.address, name='address'),
    path('orders/', views.orders, name='orders'),


    path('changepassword/', auth_view.PasswordChangeView.as_view(template_name='app/changepassword.html',form_class=ChangePassword,success_url='/changepassworddone/'), name='changepassword'),
    path('changepassworddone/',TemplateView.as_view(template_name='app/passwordchangedone.html'), name='changepassworddone'),
   
   
   
    path('mobile/', views.mobile, name='mobile'),
    path('mobile/<slug:data>', views.mobile, name='mobiledata'),

   
    path('login/', auth_view.LoginView.as_view(template_name="app/login.html",authentication_form=LoginForm), name='login'),
    path('logout/', auth_view.LogoutView.as_view(next_page='login'), name='logout'),
    path('registration/', views.Registration.as_view(), name='customerregistration'),
   
   
    path('password_reset/', auth_view.PasswordResetView.as_view(template_name='app/resetpassword.html',form_class=ResetForm), name='password_reset'),
    path('password_reset/done/',TemplateView.as_view(template_name='app/resetdone.html'), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', auth_view.PasswordResetConfirmView.as_view(template_name='app/resetconfirm.html', form_class=SetPassword), name='password_reset_confirm'),
    path('reset/done/', TemplateView.as_view(template_name='app/resetcomplete.html'), name='password_reset_complete'),
   
   
    path('checkout/', views.checkout, name='checkout'),
    path('paymentdone/', views.paymentdone, name='paymentdone'),
    
]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)