from django.shortcuts import render,redirect
from .models import Cart,Product,OrderPlace,Customer
from django.views import View
from .forms import RegistrationForm,ProfileForm
from django.contrib import messages
from django.db.models import Q
from django.http import JsonResponse,HttpResponseRedirect


class Home(View):
    def get(self,request):
        topwear=Product.objects.all().filter(category='TW')
        buttomwear=Product.objects.all().filter(category='BW')
        mobile=Product.objects.all().filter(category='M')
        return render(request, 'app/home.html',{'topwear':topwear,'buttomwear':buttomwear,'mobile':mobile})

class ProductDetail(View):
    def get(self,request,pk):
        item=Product.objects.get(pk=pk)
        exist=False
        exist=Cart.objects.filter(product=item,user=request.user).exists()
        return render(request, 'app/productdetail.html',{'item':item,'exist':exist})

def add_to_cart(request):
 if request.user.is_authenticated:
    prod_id=request.GET.get('prod_id')
    product=Product.objects.get(id=prod_id)
    Cart(user=request.user,product=product).save()
    return redirect('/cart')

def showcart(request):
    if request.user.is_authenticated:
        cart=Cart.objects.filter(user=request.user)
        cart_product=[p for p in Cart.objects.all() if p.user==request.user]
        amount=0.0
        shipping_amount=70.0
        total_amount=0.0
        if cart_product:
          for p in cart_product:
              amount+=(p.quantity*p.product.discounted_price)
          total_amount=amount+shipping_amount
          return render(request, 'app/addtocart.html',{'carts':cart,'amount':amount,'total_amount':total_amount})
        else:
            return render(request,'app/empty.html')

def pluscart(request):
    if request.method=="GET":
        prod_id=request.GET.get("prod_id")
        c=Cart.objects.get(Q(product=prod_id) & Q(user=request.user))
        c.quantity+=1
        c.save()
        cart_product=[p for p in Cart.objects.all() if p.user==request.user]
        amount=0.0
        shipping_amount=70.0
        total_amount=0.0
        
        for p in cart_product:
              amount+=(p.quantity*p.product.discounted_price)
        total_amount=amount+shipping_amount
        
        data={
            'quantity':c.quantity,
            'amount':amount,
            'totalamount':total_amount
            }
        return JsonResponse(data)

def minuscart(request):
    if request.method=="GET":
        prod_id=request.GET.get("prod_id")
        c=Cart.objects.get(Q(product=prod_id) & Q(user=request.user))
        if c.quantity > 1:
            c.quantity-=1
            c.save()
            cart_product=[p for p in Cart.objects.all() if p.user==request.user]
            amount=0.0
            shipping_amount=70.0
            total_amount=0.0
            
            for p in cart_product:
                amount+=(p.quantity*p.product.discounted_price)
            total_amount=amount+shipping_amount
            
            data={
                'quantity':c.quantity,
                'amount':amount,
                'totalamount':total_amount
                }
            return JsonResponse(data)

def removecart(request):
    if request.method=="GET":
        prod_id=request.GET.get("prod_id")
        c=Cart.objects.get(Q(product=prod_id) & Q(user=request.user))
       
        c.delete()
        cart_product=[p for p in Cart.objects.all() if p.user==request.user]
        amount=0.0
        shipping_amount=70.0
        total_amount=0.0
            
        for p in cart_product:
            amount+=(p.quantity*p.product.discounted_price)
        total_amount=amount+shipping_amount
            
        data={
                'amount':amount,
                'totalamount':total_amount
            }
    
        return JsonResponse(data)

def buy_now(request):
 return render(request, 'app/buynow.html')

def address(request):
 add=Customer.objects.filter(user=request.user)
 return render(request, 'app/address.html',{'add':add,'address':'bg-primary text-white'})

def orders(request):
    op=OrderPlace.objects.filter(user=request.user)
    return render(request, 'app/orders.html',{'op':op})

def change_password(request):
 return render(request, 'app/changepassword.html')

    

def mobile(request,data=None):
    if data==None:
        mobiles=Product.objects.filter(category='M')
    elif data=='Samsung' or data=='Realme':
        mobiles=Product.objects.filter(category='M').filter(brand=data)
    elif data=='below':
        mobiles=Product.objects.filter(category='M').filter(discounted_price__lte=10000)
    elif data=='above':
        mobiles=Product.objects.filter(category='M').filter(discounted_price__gt=10000)
    return render(request, 'app/mobile.html',{'mobiles':mobiles})


class Registration(View):
    def get(self,request):
        fm=RegistrationForm()
        return render(request,'app/customerregistration.html',{'form':fm})
    def post(self,request):
        fm=RegistrationForm(request.POST)
        if fm.is_valid():
            messages.success(request,'Congratulations! Registered successfully')
            fm.save()
        return render(request,'app/customerregistration.html',{'form':fm})

def checkout(request):
    user=request.user
    add=Customer.objects.filter(user=user)
    cart_item=Cart.objects.filter(user=user)
    cart_product=[p for p in Cart.objects.all() if p.user==request.user]
    amount=0.0
    shipping_amount=70.0
    total_amount=0.0
    if cart_product:            
        for p in cart_product:
            amount+=(p.quantity*p.product.discounted_price)
    total_amount=amount+shipping_amount
    return render(request, 'app/checkout.html',{'add':add,'totalamount':total_amount,'items':cart_item})


def paymentdone(request):
    cusid=request.GET.get('cusid')
    user=request.user
    item=Cart.objects.filter(user=user)
    customer=Customer.objects.get(id=cusid)
    for c in item:
        OrderPlace(user=user,customer=customer,product=c.product,quantity=c.quantity).save()
        c.delete()
    return redirect("orders")

class Profile(View):
    def get(self,request):
        form=ProfileForm()
        return render(request,'app/profile.html',{'form':form,'profile':'bg-primary text-white'})
    def post(self,request):
        form=ProfileForm(request.POST)
        if form.is_valid():
            name=form.cleaned_data['name']
            locality=form.cleaned_data['locality']
            city=form.cleaned_data['city']
            state=form.cleaned_data['state']
            zipcode=form.cleaned_data['zipcode']
            user=Customer(user=request.user ,name=name,locality=locality,city=city,state=state,zipcode=zipcode)
            messages.success(request,"Congratulations profile saved successfully")
            user.save()
            form=ProfileForm()
        return render(request,'app/profile.html',{'form':form,'profile':'bg-primary text-white'})

